package com.example.virtualwallet.helpers;

import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;

public class CardHelpers {
    public static Card createMockCard() {
        Card mockCard = new Card();
        mockCard.setId(1);
        mockCard.setCvv("123");
        mockCard.setCardHolder("John Doe");
        mockCard.setExpirationDate("09/25");
        mockCard.setCardNumber("1234 5678 9101 1121");

        User mockUser = UserHelpers.createMockUser();
        mockCard.setUser(mockUser);

        return mockCard;
    }
}
