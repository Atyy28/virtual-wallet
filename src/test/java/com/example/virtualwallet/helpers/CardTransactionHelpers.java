package com.example.virtualwallet.helpers;

import com.example.virtualwallet.models.CardTransaction;

import java.time.LocalDateTime;

import static com.example.virtualwallet.helpers.UserHelpers.createMockUser;

public class CardTransactionHelpers {
    public static CardTransaction createMockCardTransaction() {
        CardTransaction mockCardTransaction = new CardTransaction();
        mockCardTransaction.setId(1);
        mockCardTransaction.setDateTime(LocalDateTime.now());
        mockCardTransaction.setAmount(100.0);
        mockCardTransaction.setRecipient(createMockUser());
        return mockCardTransaction;
    }
}
