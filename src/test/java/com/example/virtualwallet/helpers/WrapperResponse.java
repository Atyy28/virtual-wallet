package com.example.virtualwallet.helpers;

import okhttp3.Response;
import okhttp3.ResponseBody;

import java.io.IOException;

public class WrapperResponse {
    private final Response response;

    public WrapperResponse(Response response) {
        this.response = response;
    }
    public ResponseBody body() {
        return response.body();
    }

    public boolean isSuccessful() {
        return response.isSuccessful();
    }

}
