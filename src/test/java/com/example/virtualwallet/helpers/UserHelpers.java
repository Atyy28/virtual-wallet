package com.example.virtualwallet.helpers;

import com.example.virtualwallet.models.User;

public class UserHelpers {
    public static User createMockUser() {
        User mockUser = new User();
        mockUser.setId(1);
        mockUser.setUsername("testUsername");
        mockUser.setPassword("testPassword");
        mockUser.setEmail("test@gmail.com");
        mockUser.setAdmin(false);
        return mockUser;
    }
}
