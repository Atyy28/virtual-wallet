package com.example.virtualwallet.helpers;

import com.example.virtualwallet.models.Wallet;

import static com.example.virtualwallet.helpers.UserHelpers.createMockUser;

public class WalletHelpers {
    public static Wallet createMockWallet() {
        Wallet mockWallet = new Wallet();
        mockWallet.setId(1);
        mockWallet.setMoneyBalance(1000.00);
        mockWallet.setUser(createMockUser());
        return mockWallet;
    }
}
