package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.AuthenticationFailedException;
import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityDuplicateException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.interfaces.UserRepository;
import com.example.virtualwallet.services.interfaces.WalletService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.example.virtualwallet.helpers.UserHelpers.createMockUser;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {
    public static final String MESSAGE_ERROR = "Username cannot be changed or already exists.";
    @Mock
    private UserRepository userRepository;
    @Mock
    private WalletService walletService;
    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void getAllUsers_Should_Call_Repository() {
        // Arrange
        User authenticatedUser = new User();
        authenticatedUser.setId(0);
        authenticatedUser.setEmail("authenticated@example.com");
        authenticatedUser.setUsername("authenticatedUser");
        authenticatedUser.setAdmin(true);

        User user1 = createMockUser();
        user1.setId(1);
        user1.setEmail("user1@example.com");
        user1.setUsername("user1");

        User user2 = createMockUser();
        user2.setId(2);
        user2.setEmail("user2@example.com");
        user2.setUsername("user2");

        List<User> mockUserList = Arrays.asList(user1, user2);

        when(userRepository.getAllUsers())
                .thenReturn(mockUserList);

        // Act
        List<User> retrievedUsers = userService.getAllUsers(authenticatedUser);

        // Assert
        assertEquals(2, retrievedUsers.size()); // Ensure we got 2 users back
        assertEquals(user1, retrievedUsers.get(0)); // Ensure the first user is as expected
        assertEquals(user2, retrievedUsers.get(1)); // Ensure the second user is as expected
    }

    @Test
    public void getUserById_Should_Return_CorrectUser_And_Call_Repository_When_IdExists() {
        User mockUser = createMockUser();

        when(userRepository.getUserById(mockUser.getId()))
                .thenReturn(mockUser);
        User result = userService.getUserById(mockUser.getId());

        assertEquals(mockUser, result);
    }

    @Test
    public void getByUsername_Should_Call_Repository() {
        User mockUser = createMockUser();

        when(userRepository.getByUsername(mockUser.getUsername()))
                .thenReturn(mockUser);

        User result = userService.getByUsername(mockUser.getUsername());

        assertEquals(mockUser, result);
    }

    @Test
    public void getByEmail_Should_Call_Repository_WhenEmailIsProvided() {
        User mockUser = createMockUser();

        when(userRepository.getByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        User result = userService.getByEmail(mockUser.getEmail());

        assertEquals(mockUser, result);
    }

    @Test
    public void getByPhoneNumber_Should_Call_Repository_And_Return_CorrectUser_When_PhoneNumberIsProvided() {
        User mockUser = createMockUser();

        when(userRepository.getByPhoneNumber(mockUser.getPhoneNumber()))
                .thenReturn(mockUser);

        User result = userService.getByPhoneNumber(mockUser.getPhoneNumber());

        assertEquals(mockUser, result);
    }

    @Test
    public void getUserByIdentifier_Should_Call_Repository_And_Return_CorrectUser_When_IdentifierIsProvided() {
        String testIdentifier = "testUsername";
        User mockUser = new User();
        mockUser.setUsername(testIdentifier);

        when(userRepository.findByUsernameOrEmailOrPhoneNumber(testIdentifier, testIdentifier, testIdentifier))
                .thenReturn(Optional.of(mockUser));

        User foundUser = userService.getUserByIdentifier(testIdentifier);

        assertNotNull(foundUser);
        assertEquals(testIdentifier, foundUser.getUsername());
    }

    @Test
    public void getAllUsers_Should_Throw_AuthorizationException_If_Not_Admin() {
        // Arrange
        User authenticatedUser = new User();
        authenticatedUser.setAdmin(false);

        // Act & Assert
        assertThrows(AuthorizationException.class,
                () -> {
                    userService.getAllUsers(authenticatedUser);
                });
    }

    @Test
    public void createUser_Should_Call_Repository_When_UserIsWithExistingEmail() {
        User mockUser = createMockUser();

        when(userRepository.getByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        assertThrows(EntityDuplicateException.class,
                () -> userService.create(mockUser));

        verify(userRepository, times(1))
                .getByEmail(mockUser.getEmail());
    }

    @Test
    public void createUser_Should_Call_Repository_When_UserIsWithExistingUsername() {
        User mockUser = createMockUser();

        when(userRepository.getByEmail(mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        when(userRepository.getByUsername(mockUser.getUsername())).thenReturn(mockUser);

        assertThrows(EntityDuplicateException.class,
                () -> userService.create(mockUser));
    }

    @Test
    public void create_Should_Call_Repository() {
        User mockUser = createMockUser();
        mockUser.setEmail("test@gmail.com");
        mockUser.setUsername("testUsername");

        when(userRepository.getByEmail(mockUser.getEmail()))
                .thenThrow(new EntityNotFoundException("User", "email", mockUser.getEmail()));
        when(userRepository.getByUsername(mockUser.getUsername()))
                .thenThrow(new EntityNotFoundException("User", "username", mockUser.getUsername()));
        doNothing().when(userRepository)
                .create(any(User.class));
        doNothing().when(walletService)
                .create(any(Wallet.class));

        userService.create(mockUser);

        verify(userRepository, times(1))
                .create(mockUser);
    }

    @Test
    public void update_Should_Call_Repository() {
        User existingUser = createMockUser();

        User updatedInfo = new User();
        updatedInfo.setId(existingUser.getId());
        updatedInfo.setUsername(existingUser.getUsername());
        updatedInfo.setEmail("newemail@example.com");
        updatedInfo.setFirstName("New First Name");
        updatedInfo.setLastName("New Last Name");
        updatedInfo.setPhoneNumber("1234567890");

        User authenticatedUser = new User();
        authenticatedUser.setBlocked(false);
        authenticatedUser.setAdmin(false);

        when(userRepository.getUserById(existingUser.getId()))
                .thenReturn(existingUser);
        doNothing().when(userRepository)
                .update(any(User.class));

        userService.update(updatedInfo, authenticatedUser);

        verify(userRepository, times(1))
                .update(any(User.class));
    }

    @Test
    public void update_Should_Call_Repository_When_UserIsWithChangedUsername_And_DoNotUpdate_When_AuthenticationFailed() {
        User existingUser = createMockUser();
        existingUser.setUsername("originalUsername");

        User updatedInfo = new User();
        updatedInfo.setId(existingUser.getId());
        updatedInfo.setUsername("newUsername");

        User authenticatedUser = new User();
        authenticatedUser.setBlocked(false);
        authenticatedUser.setAdmin(false);

        when(userRepository.getUserById(existingUser.getId()))
                .thenReturn(existingUser);

        AuthenticationFailedException exception = assertThrows(AuthenticationFailedException.class,
                () -> userService.update(updatedInfo, authenticatedUser));

        assertEquals(MESSAGE_ERROR, exception.getMessage());

        verify(userRepository, never())
                .update(any(User.class));
    }

    @Test
    public void blockUser_Should_Call_Repository() {
        User mockUser = createMockUser();
        User adminUser = createMockUser();
        adminUser.setAdmin(true);

        doNothing().when(userRepository)
                .update(any(User.class));

        userService.blockUser(mockUser, adminUser);

        verify(userRepository, times(1))
                .update(mockUser);
    }

    @Test
    public void unblockUser_Should_Call_Repository() {
        User mockUser = createMockUser();
        User adminUser = createMockUser();
        adminUser.setAdmin(true);

        doNothing().when(userRepository)
                .update(any(User.class));

        userService.unblockUser(mockUser, adminUser);

        verify(userRepository, times(1))
                .update(mockUser);
    }

    @Test
    public void promoteToAdmin_Should_Call_Repository() {
        User mockUser = createMockUser();
        User adminUser = createMockUser();
        adminUser.setAdmin(true);

        doNothing().when(userRepository)
                .update(any(User.class));

        userService.promoteToAdmin(mockUser, adminUser);

        verify(userRepository, times(1))
                .update(mockUser);
    }

    @Test
    public void deleteUser_Should_Call_Repository() {
        // Arrange
        int mockUserId = 1;
        User mockAuthenticatedUser = new User();
        mockAuthenticatedUser.setId(mockUserId);

        User mockExistingUser = new User();
        mockExistingUser.setId(mockUserId);
        mockExistingUser.setDeactivated(false);

        when(userRepository.getUserById(mockUserId))
                .thenReturn(mockExistingUser);

        // Act
        userService.delete(mockUserId, mockAuthenticatedUser);

        // Assert
        verify(userRepository, times(1))
                .getUserById(mockUserId);

        assertTrue(mockExistingUser.isDeactivated());

        verify(userRepository, times(1))
                .update(mockExistingUser);
    }

    @Test
    public void searchUser_Should_Throw_AuthorizationException_When_Not_Admin() {
        User authenticatedUser = createMockUser();
        authenticatedUser.setAdmin(false);

        assertThrows(AuthorizationException.class,
                () -> userService.searchUser("username", "email", "phoneNumber", authenticatedUser));
    }

    @Test
    public void searchUser_Should_Return_User_When_Found() {
        User authenticatedUser = createMockUser();
        authenticatedUser.setAdmin(true);

        User foundUser = createMockUser();
        String username = "username";
        String email = "email";
        String phoneNumber = "phoneNumber";

        when(userRepository.findByUsernameOrEmailOrPhoneNumber(username, email, phoneNumber))
                .thenReturn(Optional.of(foundUser));

        User result = userService.searchUser(username, email, phoneNumber, authenticatedUser);

        assertNotNull(result);
        assertEquals(foundUser, result);
    }

    @Test
    public void searchUser_Should_Throw_EntityNotFoundException_When_Not_Found() {
        User authenticatedUser = createMockUser();
        authenticatedUser.setAdmin(true);

        String username = "username";
        String email = "email";
        String phoneNumber = "phoneNumber";

        when(userRepository.findByUsernameOrEmailOrPhoneNumber(username, email, phoneNumber))
                .thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class,
                () -> userService.searchUser(username, email, phoneNumber, authenticatedUser));
    }

    @Test
    public void promoteToAdmin_Should_Throw_AuthorizationException_If_User_Is_Not_Admin() {
        // Arrange
        User userToBePromoted = new User();
        userToBePromoted.setAdmin(false);

        User authenticatedUser = new User();
        authenticatedUser.setAdmin(false);

        // Act & Assert
        assertThrows(AuthorizationException.class,
                () -> {
                    userService.promoteToAdmin(userToBePromoted, authenticatedUser);
                });
    }

    @Test
    public void blockUser_Should_Throw_AuthorizationException_When_UserIsNotAdmin() {
        User mockUser = createMockUser();
        User notAdminUser = createMockUser();
        notAdminUser.setAdmin(false);

        assertThrows(AuthorizationException.class,
                () -> userService.blockUser(mockUser, notAdminUser));
    }

}