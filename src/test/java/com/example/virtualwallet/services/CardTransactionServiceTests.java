package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.helpers.UserHelpers;
import com.example.virtualwallet.helpers.WrapperResponse;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.CardTransaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.interfaces.CardTransactionRepository;
import com.example.virtualwallet.repositories.interfaces.WalletRepository;
import com.example.virtualwallet.services.interfaces.CardTransactionService;
import okhttp3.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CardTransactionServiceTests {
    @InjectMocks
    private CardTransactionServiceImpl cardTransactionService;
    @Mock
    private CardTransactionRepository cardTransactionRepository;

    @Test
    public void testGetAllCardTransactions_Success() {
        CardTransaction transaction = new CardTransaction();
        when(cardTransactionRepository.getAllCardTransactions())
                .thenReturn(Arrays.asList(transaction));

        List<CardTransaction> transactions = cardTransactionService.getAllCardTransactions();
        assertNotNull(transactions);
        assertFalse(transactions.isEmpty());
    }

    @Test
    public void testGetAllCardTransactions_Failure() {
        when(cardTransactionRepository.getAllCardTransactions())
                .thenReturn(Collections.emptyList());

        assertThrows(EntityNotFoundException.class,
                () -> cardTransactionService.getAllCardTransactions());
    }

    @Test
    public void testGetCardTransactionById_Success() {
        CardTransaction transaction = new CardTransaction();
        when(cardTransactionRepository.getCardTransactionById(1))
                .thenReturn(transaction);

        CardTransaction result = cardTransactionService.getCardTransactionById(1);
        assertNotNull(result);
    }

    @Test
    public void testGetCardTransactionById_Failure() {
        when(cardTransactionRepository.getCardTransactionById(1)).thenReturn(null);

        assertThrows(EntityNotFoundException.class,
                () -> cardTransactionService.getCardTransactionById(1));
    }

    @Test
    public void testGetCardTransactionByRecipientId_Success() {
        CardTransaction transaction = new CardTransaction();
        when(cardTransactionRepository.getCardTransactionByRecipientId(1))
                .thenReturn(Arrays.asList(transaction));

        List<CardTransaction> transactions = cardTransactionService.getCardTransactionByRecipientId(1);

        verify(cardTransactionRepository).getCardTransactionByRecipientId(1);

        assertNotNull(transactions);
        assertFalse(transactions.isEmpty());
    }

    @Test
    public void testGetCardTransactionByRecipientId_Failure() {
        when(cardTransactionRepository.getCardTransactionByRecipientId(1))
                .thenReturn(Collections.emptyList());

        assertThrows(EntityNotFoundException.class,
                () -> cardTransactionService.getCardTransactionByRecipientId(1));
    }

}


