package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityDuplicateException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.helpers.CardHelpers;
import com.example.virtualwallet.helpers.UserHelpers;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.repositories.interfaces.CardRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CardServiceTests {
    @Mock
    private CardRepository cardRepository;

    @InjectMocks
    private CardServiceImpl cardService;

    @Test
    public void getAllCards_Should_Call_Repository() {
        List<Card> mockCards = Arrays.asList(CardHelpers.createMockCard());
        when(cardRepository.getAllCards())
                .thenReturn(mockCards);

        List<Card> returnedCards = cardService.getAllCards();
        assertEquals(returnedCards.size(), 1);
    }

    @Test
    public void getAllCards_Should_Call_Repository_WhenNoCardsFound() {
        when(cardRepository.getAllCards())
                .thenReturn(null);

        assertThrows(EntityNotFoundException.class,
                () -> cardService.getAllCards());
    }

    @Test
    public void getCardById_Should_ReturnCard_When_CorrectId() {
        Card mockCard = CardHelpers.createMockCard();
        when(cardRepository.getCardById(1))
                .thenReturn(mockCard);

        Card returnedCard = cardService.getCardById(1);
        assertEquals(mockCard.getId(), returnedCard.getId());
    }

    @Test
    public void getCardByUserId_Should_ReturnCorrectCard() {
        Card mockCard = CardHelpers.createMockCard();
        User mockUser = UserHelpers.createMockUser();
        when(cardRepository.getCardByUserId(1))
                .thenReturn(mockCard);

        Card returnedCard = cardService.getCardByUserId(mockUser, 1);
        assertEquals(mockCard.getId(), returnedCard.getId());
    }

    @Test
    public void getCardByUserId_When_AccessUnauthorized() {
        Card mockCard = CardHelpers.createMockCard();
        User mockUser = UserHelpers.createMockUser();
        mockUser.setId(2);

        assertThrows(AuthorizationException.class,
                () -> cardService.getCardByUserId(mockUser, 1));
    }

    @Test
    public void create_Should_Call_Repository_With_Correct_User_And_Card() {
        Card mockCard = CardHelpers.createMockCard();
        User mockUser = UserHelpers.createMockUser();

        cardService.create(mockCard, mockUser);

        verify(cardRepository, times(1))
                .create(mockCard);
    }

    @Test
    public void delete_Should_Call_Repository() {
        Card mockCard = CardHelpers.createMockCard();
        User mockUser = UserHelpers.createMockUser();

        String mockUserFullName = mockUser.getFirstName() + " " + mockUser.getLastName();
        mockCard.setCardHolder(mockUserFullName);

        when(cardRepository.getCardById(1))
                .thenReturn(mockCard);

        cardService.delete(1, mockUser);

        verify(cardRepository, times(1))
                .delete(1);
    }

    @Test
    public void update_Should_Call_Repository_When_CardNumberDuplicate() {
        Card mockCard = CardHelpers.createMockCard();
        User mockUser = UserHelpers.createMockUser();
        String mockUserFullName = mockUser.getFirstName() + " " + mockUser.getLastName();
        mockCard.setCardHolder(mockUserFullName);
        Card anotherCard = CardHelpers.createMockCard();
        anotherCard.setId(2);

        when(cardRepository.getCardById(1)).thenReturn(mockCard);
        when(cardRepository.getCardByCardNumber(mockCard.getCardNumber()))
                .thenReturn(anotherCard);

        assertThrows(EntityDuplicateException.class,
                () -> cardService.update(1, mockCard, mockUser));
    }

    @Test
    public void update_Should_Call_Repository_When_CardNotFound() {
        Card mockCard = CardHelpers.createMockCard();
        User mockUser = UserHelpers.createMockUser();

        when(cardRepository.getCardById(1))
                .thenReturn(null);

        assertThrows(EntityNotFoundException.class,
                () -> cardService.update(1, mockCard, mockUser));
    }

    @Test
    public void delete_Should_Call_Repository_When_CardNotFound() {
        User mockUser = UserHelpers.createMockUser();

        when(cardRepository.getCardById(1))
                .thenReturn(null);

        assertThrows(EntityNotFoundException.class,
                () -> cardService.delete(1, mockUser));
    }

    @Test
    public void update_Should_Call_Repository_When_UnauthorizedUser() {
        Card mockCard = CardHelpers.createMockCard();
        User mockUser = UserHelpers.createMockUser();
        mockCard.setCardHolder("Unauthorized User");

        when(cardRepository.getCardById(1))
                .thenReturn(mockCard);

        assertThrows(AuthorizationException.class,
                () -> cardService.update(1, mockCard, mockUser));
    }

    @Test
    public void delete_Should_Call_Repository_When_UnauthorizedUser() {
        Card mockCard = CardHelpers.createMockCard();
        User mockUser = UserHelpers.createMockUser();
        mockCard.setCardHolder("Unauthorized User");

        when(cardRepository.getCardById(1))
                .thenReturn(mockCard);

        assertThrows(AuthorizationException.class,
                () -> cardService.delete(1, mockUser));
    }

    @Test
    public void checkModifyPermissions_CardNotFound() {
        User mockUser = UserHelpers.createMockUser();

        when(cardRepository.getCardById(1))
                .thenReturn(null);

        assertThrows(EntityNotFoundException.class,
                () -> cardService.delete(1, mockUser));
    }

    @Test
    public void getAllCards_Should_Call_Repository_When_ThereIsMultipleCards() {
        List<Card> mockCards = Arrays.asList(CardHelpers.createMockCard(),
                CardHelpers.createMockCard());
        when(cardRepository.getAllCards())
                .thenReturn(mockCards);

        List<Card> returnedCards = cardService.getAllCards();
        assertEquals(2, returnedCards.size());
    }

}
