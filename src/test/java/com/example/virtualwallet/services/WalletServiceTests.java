package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.interfaces.WalletRepository;
import com.example.virtualwallet.helpers.UserHelpers;
import com.example.virtualwallet.helpers.WalletHelpers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class WalletServiceTests {
    @Mock
    private WalletRepository mockWalletRepository;
    @InjectMocks
    private WalletServiceImpl walletService;

    @Test
    public void getWalletById_ShouldReturnWallet_WhenUserOwnsTheWallet() {
        int walletId = 1;
        int userId = 1;

        Wallet mockWallet = new Wallet();
        User mockUser = new User();
        mockUser.setId(userId);
        mockWallet.setUser(mockUser);

        when(mockWalletRepository.getWalletById(walletId))
                .thenReturn(mockWallet);

        Wallet result = walletService.getWalletById(walletId, userId);

        assertEquals(mockWallet, result);
    }

    @Test
    public void getWalletById_Should_ThrowAuthorizationException_When_UserNotOwnTheWallet() {
        int walletId = 1;
        int userId = 1;
        int anotherUserId = 2;

        Wallet mockWallet = new Wallet();
        User mockUser = new User();
        mockUser.setId(anotherUserId);
        mockWallet.setUser(mockUser);

        when(mockWalletRepository.getWalletById(walletId))
                .thenReturn(mockWallet);

        assertThrows(AuthorizationException.class, () -> {
            walletService.getWalletById(walletId, userId);
        });
    }

    @Test
    public void getWalletByUserId_Should_Return_Wallet_When_UserIsAuthenticated() {
        User authenticatedUser = new User();
        authenticatedUser.setId(1);

        Wallet mockWallet = new Wallet();
        mockWallet.setUser(authenticatedUser);

        when(mockWalletRepository.getWalletByUserId(authenticatedUser
                .getId())).thenReturn(mockWallet);

        Wallet result = walletService.getWalletByUserId(authenticatedUser, authenticatedUser.getId());

        assertEquals(mockWallet, result);
    }

    @Test
    public void getWalletByUserId_Should_ThrowAuthorizationException_When_UserIsNotAuthenticated() {
        User authenticatedUser = new User();
        authenticatedUser.setId(1);

        User anotherUser = new User();
        anotherUser.setId(2);

        assertThrows(AuthorizationException.class,
                () -> {walletService.getWalletByUserId(authenticatedUser, anotherUser.getId());
        });
    }

    @Test
    public void update_CallRepository_When_UserOwnsTheWallet() {
        Wallet mockWallet = WalletHelpers.createMockWallet();
        User mockUser = UserHelpers.createMockUser();

        int userId = mockUser.getId();
        int walletId = mockWallet.getId();

        when(mockWalletRepository.getWalletById(walletId))
                .thenReturn(mockWallet);

        walletService.update(mockWallet, userId);

        System.out.println(mockingDetails(mockWalletRepository).printInvocations());

        verify(mockWalletRepository, times(1))
                .update(mockWallet);
    }


    @Test
    public void delete_Should_CallRepository_When_UserOwnsTheWallet() {
        int walletId = 1;
        int userId = 1;

        Wallet mockWallet = new Wallet();
        User mockUser = new User();
        mockUser.setId(userId);
        mockWallet.setUser(mockUser);

        when(mockWalletRepository.getWalletById(walletId))
                .thenReturn(mockWallet);

        walletService.delete(walletId, userId);

        verify(mockWalletRepository, times(1))
                .delete(walletId);
    }

    @Test
    public void getWalletById_When_ValidUser() {
        Wallet mockWallet = WalletHelpers.createMockWallet();

        when(mockWalletRepository.getWalletById(100))
                .thenReturn(mockWallet);

        walletService.getWalletById(100, 1);

        verify(mockWalletRepository, times(2))
                .getWalletById(100);
    }

    @Test
    public void getWalletByUserId_When_ValidUser() {
        Wallet mockWallet = WalletHelpers.createMockWallet();

        when(mockWalletRepository.getWalletByUserId(mockWallet.getUser().getId()))
                .thenReturn(mockWallet);

        walletService.getWalletByUserId(mockWallet.getUser(), mockWallet.getUser().getId());

        verify(mockWalletRepository, times(1))
                .getWalletByUserId(mockWallet.getUser().getId());
    }

    @Test
    public void getWalletByUserId_When_InvalidUser() {
        Wallet mockWallet = WalletHelpers.createMockWallet();
        try {
            walletService.getWalletByUserId(mockWallet.getUser(), 2);  // Assuming 2 is an invalid user ID
        } catch (AuthorizationException e) {

        }
        verify(mockWalletRepository, never())
                .getWalletByUserId(2);
    }

    @Test
    public void getWalletByUserId_When_NoWalletFound() {
        Wallet mockWallet = WalletHelpers.createMockWallet();

        when(mockWalletRepository.getWalletByUserId(mockWallet.getUser().getId()))
                .thenReturn(null);
        try {
            walletService.getWalletByUserId(mockWallet.getUser(), mockWallet.getUser().getId());
        } catch (EntityNotFoundException e) {

        }
    }

    @Test
    public void create_Should_CallRepository() {
        Wallet mockWallet = WalletHelpers.createMockWallet();

        walletService.create(mockWallet);

        verify(mockWalletRepository, times(1))
                .create(mockWallet);
    }

    @Test
    public void update_Should_CallRepository_When_ValidUser() {
        Wallet mockWallet = WalletHelpers.createMockWallet();

        when(mockWalletRepository.getWalletById(mockWallet.getId()))
                .thenReturn(mockWallet);

        walletService.update(mockWallet, mockWallet.getUser().getId());

        verify(mockWalletRepository, times(1))
                .update(mockWallet);
    }

    @Test
    public void delete_Should_CallRepository_WhenWithValidUser() {
        Wallet mockWallet = WalletHelpers.createMockWallet();

        when(mockWalletRepository.getWalletById(mockWallet.getId()))
                .thenReturn(mockWallet);

        walletService.delete(mockWallet.getId(), mockWallet.getUser().getId());

        verify(mockWalletRepository, times(1))
                .delete(mockWallet.getId());
    }
}
