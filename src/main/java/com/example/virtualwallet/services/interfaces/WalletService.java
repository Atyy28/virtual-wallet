package com.example.virtualwallet.services.interfaces;

import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;

import java.util.List;

public interface WalletService {
    List<Wallet> getAllWallets();
    Wallet getWalletById(int id, int userId);
    Wallet getWalletByUserId(User authenticatedUser, int userId);
    void create(Wallet wallet);
    void update(Wallet wallet, int userId);
    void delete(int id, int userId);
}