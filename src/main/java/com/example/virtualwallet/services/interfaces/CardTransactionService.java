package com.example.virtualwallet.services.interfaces;

import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.CardTransaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;

import java.util.List;

public interface CardTransactionService {
    CardTransaction getCardTransactionById(int id);
    List<CardTransaction> getAllCardTransactions();
    List<CardTransaction> getCardTransactionByRecipientId(int recipientId);
    CardTransaction deposit(User user, Wallet wallet, Card card, double amount);
}