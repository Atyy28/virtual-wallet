package com.example.virtualwallet.services.interfaces;

import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;

import java.util.List;

public interface CardService {
    List<Card> getAllCards();
    Card getCardById(int id);
    Card getCardByUserId(User authenticatedUser, int userId);
    void create(Card card, User user);
    void update(int id,Card card, User user);
    void delete(int id, User user);
}