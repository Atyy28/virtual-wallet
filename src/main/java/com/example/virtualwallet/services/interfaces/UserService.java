package com.example.virtualwallet.services.interfaces;

import com.example.virtualwallet.models.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers(User authenticatedUser);
    User getUserById(int id);
    User getByUsername(String username);
    User getByEmail(String email);
    User getByPhoneNumber(String phoneNumber);
    User create(User user);
    User update(User user, User authenticatedUser);
    void promoteToAdmin(User user, User admin);
    void delete(int id, User authenticatedUser);
    void blockUser(User user, User admin);
    void unblockUser(User user, User admin);
    User getUserByIdentifier(String identifier);
    User searchUser(String username, String email, String phoneNumber, User authenticatedUser);
}