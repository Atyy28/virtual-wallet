package com.example.virtualwallet.services.interfaces;

import com.example.virtualwallet.helpers.FilterOptions;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;

import java.util.List;

public interface TransactionService {
    List<Transaction> getAllTransactionsForUser(String username, FilterOptions filterOptions);
    List<Transaction> getAllTransactions(User authenticatedUser, FilterOptions filterOptions);
    Transaction getById(int transactionId);
    void create(User sender, User recipient, double amount, Transaction transaction);
}