package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityDuplicateException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.repositories.interfaces.CardRepository;
import com.example.virtualwallet.services.interfaces.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.List;

@Service
public class CardServiceImpl implements CardService {
    private static final String MODIFY_CARD_ERROR_MESSAGE = "Changes to the card's information can be made by the card owner.";
    public static final String INVALID_USER_MATCH_ERROR = "Unauthorized request! Authenticated user does not match provided userId.";
    public static final String MESSAGE_NOT_FOUND = "No cards found.";
    private final CardRepository cardRepository;

    @Autowired
    public CardServiceImpl(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    @Override
    public List<Card> getAllCards() {
        List<Card> cards = cardRepository.getAllCards();
        if (cards == null || cards.isEmpty()) {
            throw new EntityNotFoundException(MESSAGE_NOT_FOUND);
        }
        return cards;
    }

    @Override
    public Card getCardById(int id) {
        return cardRepository.getCardById(id);
    }

    @Override
    public Card getCardByUserId(User authenticatedUser, int userId) {
        if (authenticatedUser.getId() != userId) {
            throw new AuthorizationException(INVALID_USER_MATCH_ERROR);
        }

        Card card = cardRepository.getCardByUserId(userId);

        if (card == null) {
            throw new EntityNotFoundException("Card", userId);
        }
        return card;
    }

    @Override
    public void create(Card card, User user) {
        card.setUser(user);
        cardRepository.create(card);
    }

    @Override
    public void update(int id, Card card, User user) {
        checkModifyPermissions(id, user);
        try {
            Card existingCard = cardRepository.getCardByCardNumber(card.getCardNumber());
            if (existingCard != null && existingCard.getId() != card.getId()) {
                throw new EntityDuplicateException("Card", "card number", existingCard.getCardNumber());
            }
            cardRepository.update(card);
        } catch (NoResultException e) {
            cardRepository.update(card);
        }
    }

    @Override
    public void delete(int id, User user) {
        checkModifyPermissions(id, user);
        cardRepository.delete(id);
    }

    private void checkModifyPermissions(int cardId, User user) {
        Card card = cardRepository.getCardById(cardId);
        if (card == null) {
            throw new EntityNotFoundException(MESSAGE_NOT_FOUND);
        }

        String cardHolderFullName = user.getFirstName() + " " + user.getLastName();
        if (!card.getCardHolder().equals(cardHolderFullName)) {
            throw new AuthorizationException(MODIFY_CARD_ERROR_MESSAGE);
        }
    }
}