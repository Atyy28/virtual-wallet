package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.helpers.FilterOptions;
import com.example.virtualwallet.helpers.TransactionMapper;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.interfaces.TransactionRepository;
import com.example.virtualwallet.repositories.interfaces.UserRepository;
import com.example.virtualwallet.repositories.interfaces.WalletRepository;
import com.example.virtualwallet.services.interfaces.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    public static final String BLOCKED_MESSAGE = "You have been blocked and cannot make transactions!";
    public static final String INSUFFICIENT_BALANCE = "Insufficient balance.";
    public static final String NOT_ADMIN_MESSAGE = "You are not allowed to do this action!";
    private final TransactionRepository transactionRepository;
    private final WalletRepository walletRepository;
    private final TransactionMapper transactionMapper;
    private final UserRepository userRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository, WalletRepository walletRepository,
                                  TransactionMapper transactionMapper, UserRepository userRepository) {
        this.transactionRepository = transactionRepository;
        this.walletRepository = walletRepository;
        this.transactionMapper = transactionMapper;
        this.userRepository = userRepository;
    }

    @Override
    public List<Transaction> getAllTransactionsForUser(String username, FilterOptions filterOptions) {
        User user = userRepository.getByUsername(username);
        return transactionRepository.getAllTransactionsForUser(user, filterOptions);
    }

    @Override
    public List<Transaction> getAllTransactions(User authenticatedUser, FilterOptions filterOptions) {
        if(checkIfAdmin(authenticatedUser)){
            return transactionRepository.getAllTransactions(filterOptions);
        }else{
            throw new AuthorizationException(NOT_ADMIN_MESSAGE);
        }
    }

    @Override
    public Transaction getById(int transactionId) {
        return transactionRepository.getById(transactionId);
    }

    @Override
    public void create(User sender, User recipient, double amount, Transaction transaction) {

        if (!checkIfBlocked(sender)) {
            Wallet senderWallet = walletRepository.getWalletByUserId(sender.getId());
            Wallet recipientWallet = walletRepository.getWalletByUserId(recipient.getId());

            if (senderWallet.getMoneyBalance() >= amount) {
                senderWallet.setMoneyBalance(senderWallet.getMoneyBalance() - amount);
                recipientWallet.setMoneyBalance(recipientWallet.getMoneyBalance() + amount);

                walletRepository.update(senderWallet);
                walletRepository.update(recipientWallet);
                transactionRepository.create(transaction);
            } else {
                throw new IllegalArgumentException(INSUFFICIENT_BALANCE);
            }
        } else {
            throw new AuthorizationException(BLOCKED_MESSAGE);
        }
    }

    private boolean checkIfBlocked(User user) {
        return user.isBlocked();
    }

    private boolean checkIfAdmin(User user) {
        return user.isAdmin();
    }
}