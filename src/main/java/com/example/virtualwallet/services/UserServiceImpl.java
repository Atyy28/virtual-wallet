package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.AuthenticationFailedException;
import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityDuplicateException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.interfaces.UserRepository;
import com.example.virtualwallet.services.interfaces.UserService;
import com.example.virtualwallet.services.interfaces.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private static final String AUTHORIZATION_ADMIN_ERROR = "Only admins can use this command.";
    private static final String USERNAME_UPDATE_ERROR = "Username cannot be changed or already exists.";
    public static final String NOT_FOUND_MESSAGE = "User not found";
    public static final String NOT_AUTHORIZED_MESSAGE = "You are not authorized to deactivate this profile.";
    private final UserRepository userRepository;
    private final WalletService walletService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           WalletService walletService) {
        this.userRepository = userRepository;
        this.walletService = walletService;
    }

    @Override
    public List<User> getAllUsers(User authenticatedUser) {
        if (!authenticatedUser.isAdmin()) {
            throw new AuthorizationException(AUTHORIZATION_ADMIN_ERROR);
        }
        return userRepository.getAllUsers();
    }

    @Override
    public User getUserById(int id) {
        return userRepository.getUserById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    public User getByPhoneNumber(String phoneNumber) {
        return userRepository.getByPhoneNumber(phoneNumber);
    }

    @Override
    @Transactional
    public User create(User user) {
        if (isEmailDuplicate(user.getEmail())) {
            throw new EntityDuplicateException("User", "email", user.getEmail());
        }

        if (isUsernameDuplicate(user.getUsername())) {
            throw new EntityDuplicateException("User", "username", user.getUsername());
        }
        userRepository.create(user);
        Wallet wallet = new Wallet();
        wallet.setUser(user);
        walletService.create(wallet);
        return user;
    }

    @Override
    public User update(User user, User authenticatedUser) {

        User existingUser = userRepository.getUserById(user.getId());

        if (!existingUser.getUsername().equals(user.getUsername())) {
            throw new AuthenticationFailedException(USERNAME_UPDATE_ERROR);
        }

        existingUser.setEmail(user.getEmail());
        existingUser.setPassword(user.getPassword());
        existingUser.setFirstName(user.getFirstName());
        existingUser.setLastName(user.getLastName());
        existingUser.setPhoneNumber(user.getPhoneNumber());

        if (authenticatedUser.isBlocked()) {
            existingUser.setBlocked(true);
        }
        if (authenticatedUser.isAdmin()) {
            existingUser.setAdmin(true);
        }

        userRepository.update(existingUser);
        return existingUser;
    }

    @Override
    public void blockUser(User user, User admin) {
        if (admin.isAdmin() && !user.isAdmin()) {
            user.setBlocked(true);
            userRepository.update(user);
        } else throw new AuthorizationException(AUTHORIZATION_ADMIN_ERROR);
    }

    @Override
    public void unblockUser(User user, User admin) {
        if (admin.isAdmin() && !user.isAdmin()) {
            user.setBlocked(false);
            userRepository.update(user);
        } else throw new AuthorizationException(AUTHORIZATION_ADMIN_ERROR);
    }

    @Override
    public void promoteToAdmin(User user, User admin) {
        if (admin.isAdmin()) {
            user.setAdmin(true);
            userRepository.update(user);
        } else throw new AuthorizationException(AUTHORIZATION_ADMIN_ERROR);
    }

    private boolean isEmailDuplicate(String email) {
        try {
            userRepository.getByEmail(email);
            return true;
        } catch (EntityNotFoundException e) {
            return false;
        }
    }

    private boolean isUsernameDuplicate(String username) {
        try {
            userRepository.getByUsername(username);
            return true;
        } catch (EntityNotFoundException e) {
            return false;
        }
    }

    @Override
    public void delete(int id, User authenticatedUser) {
        if (authenticatedUser.getId() != id) {
            throw new AuthorizationException(NOT_AUTHORIZED_MESSAGE);
        }
        User existingUser = userRepository.getUserById(id);
        if (existingUser == null) {
            throw new EntityNotFoundException(NOT_FOUND_MESSAGE);
        }
        existingUser.setDeactivated(true);
        userRepository.update(existingUser);
    }

    @Override
    public User getUserByIdentifier(String identifier) {
        return userRepository.findByUsernameOrEmailOrPhoneNumber(identifier, identifier, identifier)
                .orElseThrow(() -> new EntityNotFoundException("User"));
    }

    @Override
    public User searchUser(String username, String email, String phoneNumber, User authenticatedUser) {
        if (!authenticatedUser.isAdmin()) {
            throw new AuthorizationException(AUTHORIZATION_ADMIN_ERROR);
        }

        Optional<User> optionalUser = userRepository.findByUsernameOrEmailOrPhoneNumber(username, email, phoneNumber);

        if (optionalUser.isPresent()) {
            return optionalUser.get();
        } else {
            throw new EntityNotFoundException(NOT_FOUND_MESSAGE);
        }
    }
}