package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.CardTransaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.interfaces.CardTransactionRepository;
import com.example.virtualwallet.repositories.interfaces.WalletRepository;
import com.example.virtualwallet.services.interfaces.CardTransactionService;
import okhttp3.*;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class CardTransactionServiceImpl implements CardTransactionService {
    public static final String MESSAGE_NOT_FOUND = "Card Transaction not found with ID: ";
    public static final String MESSAGE_TRANSACTION_NOT_FOUND = "No card transactions found for the recipient with ID: ";
    public static final String MESSAGE_NOT_FOUND_ERROR = "No card transactions found in the system.";
    public static final String MESSAGE_CARD_DECLINED = "Card not approved.";
    public static final String MESSAGE_DEPOSIT_API_ERROR = "Deposit API exception.";
    private final CardTransactionRepository cardTransactionRepository;
    private final WalletRepository walletRepository;

    public CardTransactionServiceImpl(CardTransactionRepository cardTransactionRepository,
                                      WalletRepository walletRepository) {
        this.cardTransactionRepository = cardTransactionRepository;
        this.walletRepository = walletRepository;
    }

    @Override
    public List<CardTransaction> getAllCardTransactions() {
        List<CardTransaction> transactions = cardTransactionRepository.getAllCardTransactions();
        if (transactions == null || transactions.isEmpty()) {
            throw new EntityNotFoundException(MESSAGE_NOT_FOUND_ERROR);
        }
        return transactions;
    }

    @Override
    public CardTransaction getCardTransactionById(int id) {
        CardTransaction transaction = cardTransactionRepository.getCardTransactionById(id);
        if (transaction == null) {
            throw new EntityNotFoundException(MESSAGE_NOT_FOUND, id);
        }
        return transaction;
    }

    @Override
    public List<CardTransaction> getCardTransactionByRecipientId(int recipientId) {
        List<CardTransaction> transactions = cardTransactionRepository.getCardTransactionByRecipientId(recipientId);
        if (transactions == null || transactions.isEmpty()) {
            throw new EntityNotFoundException(MESSAGE_TRANSACTION_NOT_FOUND, recipientId);
        }
        return transactions;
    }

    @Override
    @Transactional
    public CardTransaction deposit(User user, Wallet wallet, Card card, double amount) {
        try {
            String urlString = "http://localhost:8090/card/withdraw";
            OkHttpClient client = new OkHttpClient();
            JSONObject jsonPayload = new JSONObject();
            jsonPayload.put("number", card.getCardNumber());
            jsonPayload.put("cardHolder", card.getCardHolder());
            jsonPayload.put("expirationDate", card.getExpirationDate().toString());
            jsonPayload.put("cvv", card.getCvv());
            jsonPayload.put("amount", amount);

            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), jsonPayload.toString());
            Request request = new Request.Builder()
                    .url(urlString)
                    .post(requestBody)
                    .build();
            Response response = client.newCall(request).execute();

            if (response.isSuccessful()) {
                String responseBody = response.body().string();
                JSONObject jsonResponse = new JSONObject(responseBody);
                String responseStatus = jsonResponse.getString("response");
                if (responseStatus.equals("Successful")) {
                    CardTransaction cardTransaction = createTransactionDeposit(amount, wallet);
                    cardTransactionRepository.create(cardTransaction);
                    return cardTransaction;
                } else {
                    throw new AuthorizationException(MESSAGE_CARD_DECLINED);
                }
            } else {
                throw new AuthorizationException(MESSAGE_DEPOSIT_API_ERROR);
            }

        } catch (IOException ignored) {
            throw new EntityNotFoundException(ignored.getMessage());
        }
    }

    public CardTransaction createTransactionDeposit(double amount, Wallet recipientWallet) {
        CardTransaction depositTransaction = new CardTransaction();
        depositTransaction.setDateTime(LocalDateTime.now());
        depositTransaction.setRecipient(recipientWallet.getUser());
        depositTransaction.setAmount(amount);
        recipientWallet.setMoneyBalance(recipientWallet.getMoneyBalance() + amount);
        walletRepository.update(recipientWallet);
        return depositTransaction;
    }
}