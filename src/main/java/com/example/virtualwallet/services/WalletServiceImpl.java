package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.interfaces.WalletRepository;
import com.example.virtualwallet.services.interfaces.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WalletServiceImpl implements WalletService {
    private static final String MODIFY_WALLET_ERROR_MESSAGE = "You don't have the permission to modify this wallet.";
    public static final String INVALID_USER_MATCH_ERROR = "Unauthorized request! Authenticated user does not match provided userId.";
    public static final String NOT_FOUND_MESSAGE = "No wallets found.";
    private final WalletRepository walletRepository;

    @Autowired
    public WalletServiceImpl(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }

    @Override
    public List<Wallet> getAllWallets() {
        List<Wallet> wallets = walletRepository.getAllWallets();
        if(wallets == null || wallets.isEmpty()){
            throw new EntityNotFoundException(NOT_FOUND_MESSAGE);
        }
        return wallets;
    }

    @Override
    public Wallet getWalletById(int id, int userId) {
        checkUserOwnership(id, userId);
        return walletRepository.getWalletById(id);
    }

    @Override
    public Wallet getWalletByUserId(User authenticatedUser, int userId) {
        if (authenticatedUser.getId() != userId) {
            throw new AuthorizationException(INVALID_USER_MATCH_ERROR);
        }

        Wallet wallet = walletRepository.getWalletByUserId(userId);

        if (wallet == null) {
            throw new EntityNotFoundException("Wallet", userId);
        }
        return wallet;
    }

    @Override
    public void create(Wallet wallet) {
        walletRepository.create(wallet);
    }

    @Override
    public void update(Wallet wallet, int userId) {
        checkUserOwnership(wallet.getId(), userId);
        walletRepository.update(wallet);
    }

    @Override
    public void delete(int id, int userId) {
        checkUserOwnership(id, userId);
        walletRepository.delete(id);
    }

    private void checkUserOwnership(int walletId, int userId) {
        Wallet wallet = walletRepository.getWalletById(walletId);
        if (userId != wallet.getUser().getId()) {
            throw new AuthorizationException(MODIFY_WALLET_ERROR_MESSAGE);
        }
    }
}