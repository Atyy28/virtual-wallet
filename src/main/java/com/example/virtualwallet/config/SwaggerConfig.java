package com.example.virtualwallet.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.virtualwallet"))
                .paths(PathSelectors.ant("/api/**"))
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Virtual Wallet API")
                .description("API documentation for the Virtual Wallet application")
                .version("1.0.0")
                .contact(new Contact("Adelina Velinova, Vitorio Petkov, Atche Ahmed", "https://gitlab.com/project-3-team-12/virtual-wallet.git", "adi_velinova@abv.bg, vitoriopetkov@gmail.com, atceselim@gmail.com"))
                .build();
    }
}

