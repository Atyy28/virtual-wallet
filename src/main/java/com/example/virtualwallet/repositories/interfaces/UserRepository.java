package com.example.virtualwallet.repositories.interfaces;

import com.example.virtualwallet.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    List<User> getAllUsers();
    User getUserById(int id);
    User getByUsername(String username);
    User getByEmail(String email);
    User getByPhoneNumber(String phoneNumber);
    void create(User user);
    void update(User user);
    void delete(int id);
    Optional<User> findByUsernameOrEmailOrPhoneNumber(String username, String email, String phoneNumber);
}