package com.example.virtualwallet.repositories.interfaces;

import com.example.virtualwallet.helpers.FilterOptions;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;

import java.util.List;

public interface TransactionRepository {
    List<Transaction> getAllTransactionsForUser(User user, FilterOptions filterOptions);
    List<Transaction> getAllTransactions(FilterOptions filterOptions);
    Transaction getById(int transactionId);
    void create(Transaction transaction);
}