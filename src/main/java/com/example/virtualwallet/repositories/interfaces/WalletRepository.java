package com.example.virtualwallet.repositories.interfaces;

import com.example.virtualwallet.models.Wallet;

import java.util.List;

public interface WalletRepository {
    Wallet getWalletById(int id);

    List<Wallet> getAllWallets();

    Wallet getWalletByUserId(int userId);
    void create(Wallet wallet);
    void update(Wallet wallet);
    void delete(int id);
}
