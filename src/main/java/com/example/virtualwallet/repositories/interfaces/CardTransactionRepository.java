package com.example.virtualwallet.repositories.interfaces;

import com.example.virtualwallet.models.CardTransaction;

import java.util.List;

public interface CardTransactionRepository {
    CardTransaction getCardTransactionById(int id);
    List<CardTransaction> getAllCardTransactions();
    List<CardTransaction> getCardTransactionByRecipientId(int recipientId);
    void create(CardTransaction cardTransaction);
}