package com.example.virtualwallet.repositories.interfaces;

import com.example.virtualwallet.models.Card;

import java.util.List;

public interface CardRepository {
    List<Card> getAllCards();
    Card getCardById(int id);
    Card getCardByUserId(int userId);
    Card getCardByCardNumber(String cardNumber);
    void create(Card card);
    void update(Card card);
    void delete(int id);
}