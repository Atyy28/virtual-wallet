package com.example.virtualwallet.repositories;

import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.helpers.FilterOptions;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.repositories.interfaces.TransactionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TransactionRepositoryImpl implements TransactionRepository {

    private final SessionFactory sessionFactory;

    public TransactionRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Transaction> getAllTransactionsForUser(User user, FilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("select t from Transaction t");
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            filterOptions.getStartDate().ifPresent(value -> {
                filters.add("t.date >= :startDate");
                params.put("startDate", value.atStartOfDay());
            });

            filterOptions.getEndDate().ifPresent(value -> {
                filters.add("t.date <= :endDate");
                params.put("endDate", value.atTime(LocalTime.MAX));
            });

            queryString.append(" WHERE t.sender = :user OR t.recipient = :user ");
            System.out.println(queryString);
            if (!filters.isEmpty()) {
                queryString
                        .append(" where ")
                        .append(String.join(" and ", filters));
            }

            queryString.append(generateOrderBy(filterOptions));
            System.out.println(queryString);
            Query<Transaction> query = session.createQuery(queryString.toString(), Transaction.class);
            query.setParameter("user", user);
            System.out.println(queryString);
            return query.list();
        }
    }

    @Override
    public List<Transaction> getAllTransactions(FilterOptions filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from Transaction t");
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            filterOptions.getStartDate().ifPresent(value -> {
                filters.add("t.date >= :startDate");
                params.put("startDate", value.atStartOfDay());
            });

            filterOptions.getEndDate().ifPresent(value -> {
                filters.add("t.date <= :endDate");
                params.put("endDate", value.atTime(LocalTime.MAX));
            });

            filterOptions.getSender().ifPresent(value -> {
                filters.add("t.sender.username like :sender");
                params.put("sender", String.format("%%%s%%", value));
            });

            filterOptions.getRecipient().ifPresent(value -> {
                filters.add("t.recipient.username like :recipient");
                params.put("recipient", String.format("%%%s%%", value));
            });

            if (!filters.isEmpty()) {
                queryString
                        .append(" where ")
                        .append(String.join(" and ", filters));
            }

            queryString.append(generateOrderBy(filterOptions));

            Query<Transaction> query = session.createQuery(queryString.toString(), Transaction.class);
            query.setProperties(params);
            System.out.println(queryString);
            return query.list();
        }
    }

    @Override
    public Transaction getById(int transactionId) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.get(Transaction.class, transactionId);
            if (transaction == null) {
                throw new EntityNotFoundException("Transaction", transactionId);
            }
            return transaction;
        }
    }

    @Override
    public void create(Transaction transaction) {
        try (Session session = sessionFactory.openSession()) {
            session.save(transaction);
        }
    }

    private String generateOrderBy(FilterOptions filterOptions) {
        if (filterOptions.getSortBy().isEmpty()) {
            return "";
        }

        String orderBy = "";
        switch (filterOptions.getSortBy().get()) {
            case "date":
                orderBy = "date";
                break;
            case "sender":
                orderBy = "t.sender.username";
                break;
            case "recipient":
                orderBy = "t.recipient.username";
                break;
            default:
                orderBy = "id";
        }

        orderBy = String.format(" order by %s", orderBy);

        if (filterOptions.getSortOrder().isPresent() && filterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            orderBy = String.format("%s desc", orderBy);
        }

        return orderBy;
    }
}