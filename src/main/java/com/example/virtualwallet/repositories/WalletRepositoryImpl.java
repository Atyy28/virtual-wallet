package com.example.virtualwallet.repositories;

import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.interfaces.WalletRepository;
import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class WalletRepositoryImpl implements WalletRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public WalletRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Wallet getWalletById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Wallet wallet = session.get(Wallet.class, id);
            if (wallet == null) {
                throw new EntityNotFoundException("Wallet", id);
            }
            return wallet;
        }
    }

    @Override
    public List<Wallet> getAllWallets() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Wallet ", Wallet.class).list();
        }
    }

    @Override
    public Wallet getWalletByUserId(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Wallet> query = session.createQuery("from Wallet where user.id = :userId", Wallet.class);
            query.setParameter("userId", userId);

            Wallet wallet = query.uniqueResult();

            if (wallet == null) {
                throw new EntityNotFoundException("Wallet", "user id", String.valueOf(userId));
            }
            return wallet;
        }
    }

    @Override
    public void create(Wallet wallet) {
        try (Session session = sessionFactory.openSession()) {
            session.save(wallet);
        }
    }

    @Override
    public void update(Wallet wallet) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(wallet);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Wallet wallet = session.get(Wallet.class, id);
            if (wallet == null) {
                throw new EntityNotFoundException("Wallet", id);
            }
            session.delete(wallet);
            session.getTransaction().commit();
        }
    }
}