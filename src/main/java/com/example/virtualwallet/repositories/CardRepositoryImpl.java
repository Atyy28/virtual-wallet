package com.example.virtualwallet.repositories;

import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.repositories.interfaces.CardRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CardRepositoryImpl implements CardRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CardRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Card> getAllCards() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from Card", Card.class).list();
        }
    }

    @Override
    public Card getCardById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Card card = session.get(Card.class, id);

            if (card == null) {
                throw new EntityNotFoundException("Card", id);
            }
            return card;
        }
    }

    @Override
    public Card getCardByUserId(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Card> query = session.createQuery("from Card where user.id = :userId", Card.class);
            query.setParameter("userId", userId);

            Card card = query.uniqueResult();

            if (card == null) {
                throw new EntityNotFoundException("Card", "user id", String.valueOf(userId));
            }
            return card;
        }
    }

    @Override
    public Card getCardByCardNumber(String cardNumber) {
        try (Session session = sessionFactory.openSession()) {
            Query<Card> query = session.createQuery("from Card where cardNumber = :cardNumber", Card.class);
            query.setParameter("cardNumber", cardNumber);

            Card card = query.uniqueResult();

            if (card == null) {
                throw new EntityNotFoundException("Card", "card number", cardNumber);
            }
            return card;
        }
    }

    @Override
    public void create(Card card) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(card);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Card card) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(card);
            session.getTransaction().commit();

        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(id);
            session.getTransaction().commit();
        }
    }
}