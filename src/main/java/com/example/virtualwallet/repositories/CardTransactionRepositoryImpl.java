package com.example.virtualwallet.repositories;

import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.CardTransaction;
import com.example.virtualwallet.repositories.interfaces.CardTransactionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CardTransactionRepositoryImpl implements CardTransactionRepository {
    private final SessionFactory sessionFactory;

    public CardTransactionRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<CardTransaction> getAllCardTransactions() {
        try (Session session = sessionFactory.openSession()) {
            Query<CardTransaction> query = session.createQuery("from CardTransaction", CardTransaction.class);
            return query.list();
        }
    }

    @Override
    public CardTransaction getCardTransactionById(int id) {
        try (Session session = sessionFactory.openSession()) {
            CardTransaction transaction = session.get(CardTransaction.class, id);
            if (transaction == null) {
                throw new EntityNotFoundException("CardTransaction", id);
            }
            return transaction;
        }
    }

    @Override
    public List<CardTransaction> getCardTransactionByRecipientId(int recipientId) {
        try (Session session = sessionFactory.openSession()) {
            Query<CardTransaction> query = session.createQuery("from CardTransaction where recipient.id = :recipientId", CardTransaction.class);
            query.setParameter("recipientId", recipientId);
            return query.list();
        }
    }

    @Override
    public void create(CardTransaction cardTransaction) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(cardTransaction);
            session.getTransaction().commit();
        }
    }
}