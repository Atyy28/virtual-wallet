package com.example.virtualwallet.exceptions;

public class AuthenticationFailedException extends  RuntimeException {
    public AuthenticationFailedException(String message) {
        super(message);
    }
}
