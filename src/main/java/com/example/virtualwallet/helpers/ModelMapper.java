package com.example.virtualwallet.helpers;

import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.RegisterDto;
import org.springframework.stereotype.Component;

@Component
public class ModelMapper {
    public User fromDto(RegisterDto dto) {
        if (dto == null) {
            return null;
        }
        User user = new User();
        user.setUsername(dto.getUsername());
        user.setPassword(dto.getPassword());
        user.setEmail(dto.getEmail());
        user.setPhoneNumber(dto.getPhoneNumber());
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.setUserPhoto("https://example.com/john_doe.jpg");

        return user;
    }
}