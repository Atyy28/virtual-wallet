package com.example.virtualwallet.helpers;

import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.CardDto;
import com.example.virtualwallet.repositories.interfaces.CardRepository;
import com.example.virtualwallet.services.CardServiceImpl;
import com.example.virtualwallet.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CardMapper {
    private final CardServiceImpl cardService;
    private final CardRepository cardRepository;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CardMapper(CardServiceImpl cardService, CardRepository cardRepository, UserService userService, AuthenticationHelper authenticationHelper) {
        this.cardService = cardService;
        this.cardRepository = cardRepository;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    public static Card fromDto(CardDto cardDto) {
        Card card = new Card();
        card.setCardHolder(cardDto.getCardHolder());
        card.setCardNumber(cardDto.getCardNumber());
        card.setCvv(cardDto.getCvv());
        card.setExpirationDate(cardDto.getExpirationDate());
        return card;
    }

    public CardDto toDto(Card card) {
        CardDto cardDto = new CardDto();
        cardDto.setCardHolder(card.getCardHolder());
        cardDto.setCardNumber(card.getCardNumber());
        cardDto.setCvv(card.getCvv());
        cardDto.setExpirationDate(card.getExpirationDate());
        return cardDto;
    }
}