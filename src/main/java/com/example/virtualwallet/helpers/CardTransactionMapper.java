package com.example.virtualwallet.helpers;

import com.example.virtualwallet.models.CardTransaction;
import com.example.virtualwallet.models.dtos.CardTransactionDto;
import org.springframework.stereotype.Component;

@Component
public class CardTransactionMapper {

    public CardTransaction fromDto(CardTransactionDto transactionDto) {
        CardTransaction transaction = new CardTransaction();
        transaction.setAmount(transactionDto.getAmount());
        return transaction;
    }

    public CardTransactionDto toDto(CardTransaction transaction) {
        CardTransactionDto transactionDto = new CardTransactionDto();
        transactionDto.setAmount(transaction.getAmount());
        return transactionDto;
    }
}