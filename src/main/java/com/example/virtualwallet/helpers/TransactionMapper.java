package com.example.virtualwallet.helpers;

import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.TransactionDto;
import com.example.virtualwallet.repositories.interfaces.UserRepository;
import com.example.virtualwallet.services.interfaces.UserService;
import org.springframework.stereotype.Component;

@Component
public class TransactionMapper {
    private final UserRepository userRepository;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    public TransactionMapper(UserRepository userRepository, UserService userService,
                             AuthenticationHelper authenticationHelper)
    {
        this.userRepository = userRepository;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    public Transaction fromDto(TransactionDto transactionDto)
    {
        User recipient = userService.getUserByIdentifier(transactionDto.getRecipientIdentifier());
        Transaction transaction = new Transaction();
        transaction.setAmount(transactionDto.getAmount());
        transaction.setRecipient(recipient);
        transaction.setDate(transactionDto.getDate());
        return transaction;
    }

    public TransactionDto toDto(Transaction transaction)
    {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setAmount(transaction.getAmount());
        transactionDto.setRecipientIdentifier(transaction.getRecipient().getUsername());
        transactionDto.setDate(transaction.getDate());
        return transactionDto;
    }
}
