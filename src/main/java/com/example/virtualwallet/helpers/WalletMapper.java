package com.example.virtualwallet.helpers;

import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.dtos.WalletDto;
import com.example.virtualwallet.services.interfaces.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component

public class WalletMapper {
    private final WalletService walletService;

    @Autowired
    public WalletMapper(WalletService walletService) {
        this.walletService = walletService;
    }

    public Wallet fromDtoWithId(WalletDto walletDto) {
        Wallet wallet = fromDto(walletDto);
        wallet.setId(wallet.getId());
        return wallet;

    }

    public Wallet fromDto(WalletDto walletDto) {
        Wallet wallet = new Wallet();
        wallet.setMoneyBalance(walletDto.getMoneyBalance());
        return wallet;
    }

    public WalletDto toDto(Wallet wallet){
        WalletDto walletDto = new WalletDto();
        walletDto.setMoneyBalance(wallet.getMoneyBalance());
        return walletDto;
    }
}