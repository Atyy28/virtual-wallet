package com.example.virtualwallet.helpers;

import java.time.LocalDate;
import java.util.Optional;

public class FilterOptions {
    private Optional<LocalDate> startDate;
    private Optional<LocalDate> endDate;
    private Optional<String> sender;
    private Optional<String> recipient;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public FilterOptions() {
        this(null, null, null, null, null, null);
    }

    public FilterOptions(
            LocalDate startDate,
            LocalDate endDate,
            String sender,
            String recipient,
            String sortBy,
            String sortOrder) {
        this.startDate = Optional.ofNullable(startDate);
        this.endDate = Optional.ofNullable(endDate);
        this.sender = Optional.ofNullable(sender);
        this.recipient = Optional.ofNullable(recipient);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public Optional<LocalDate> getStartDate() {
        return startDate;
    }

    public Optional<LocalDate> getEndDate() {
        return endDate;
    }

    public Optional<String> getSender() {
        return sender;
    }

    public Optional<String> getRecipient() {
        return recipient;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }
}