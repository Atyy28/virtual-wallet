package com.example.virtualwallet.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class CardTransactionDto {

    @NotNull(message = "Amount is required")
    @Positive(message = "Amount should be positive")
    private double amount;

    public CardTransactionDto() {
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}