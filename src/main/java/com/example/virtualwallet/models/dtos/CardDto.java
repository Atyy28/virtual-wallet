package com.example.virtualwallet.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CardDto {
    @NotNull(message = "Card Holder is required")
    @Size(min = 2, max = 30, message = "Card Holder must be between 4 and 20 symbols")
    private String cardHolder;

    @NotNull(message = "Card Number is required")
    @Size(min=16,max = 16,message = "CardNumber must be 16 digits")
    private String cardNumber;

    @NotNull(message = "Cvv is required")
    @Size(min = 3, max = 3, message = "CVV code must be 3 digits")
    private String cvv;

    @NotNull(message = "Expiration date is required")
    private String expirationDate;

    public CardDto() {
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(  String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }
}