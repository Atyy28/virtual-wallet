package com.example.virtualwallet.models.dtos;
import javax.validation.constraints.NotNull;

public class WalletDto {
    @NotNull(message = "Money balance is required")
    private double moneyBalance;
    
    public WalletDto() {

    }

    public double getMoneyBalance() {
        return moneyBalance;
    }

    public void setMoneyBalance(double moneyBalance) {
        this.moneyBalance = moneyBalance;
    }
}