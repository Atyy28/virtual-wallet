package com.example.virtualwallet.controllers.mvc;


import com.example.virtualwallet.exceptions.AuthenticationFailedException;
import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityDuplicateException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.ModelMapper;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.LoginDto;
import com.example.virtualwallet.models.dtos.RegisterDto;
import com.example.virtualwallet.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/auth")
public class AuthenticationMvcController {
    public static final String WRONG_USERNAME_OR_PASSWORD_MESSAGE = "Wrong username or password.";
    private final UserService userService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public AuthenticationMvcController(UserService userService, ModelMapper modelMapper, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/login")
    public String handleLogin(Model model) {
        model.addAttribute("login", new LoginDto());
        return "LoginView";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto dto,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "LoginView";
        }
        try {
            User user = userService.getByUsername(dto.getUsername());
            if (user.isBlocked()) {
                throw new AuthorizationException("User is blocked.");
            }

            authenticationHelper.verifyAuthentication(dto.getUsername(), dto.getPassword());
            session.setAttribute("currentUser", dto.getUsername());

            if (user.isAdmin()) {
                return "redirect:/admin";
            } else {
                return "redirect:/index";
            }
        } catch (AuthenticationFailedException | AuthorizationException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "LoginView";
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("username", "auth_error", WRONG_USERNAME_OR_PASSWORD_MESSAGE);
            return "LoginView";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());
        return "RegisterView";

    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterDto register,
                                 BindingResult bindingResult,
                                 HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "RegisterView";

        }
        if (!register.getPassword().equals(register.getPassword())) {
            bindingResult.rejectValue("password", "password_error",
                    "Password confirmation should match password");
            return "RegisterView";
        }
        try {
            User user = modelMapper.fromDto(register);
            userService.create(user);
            session.setAttribute("lastRegisteredUser", user);
            return "redirect:/card/create";
        } catch (EntityDuplicateException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "RegisterView";

        }
    }
}