package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityDuplicateException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.services.WalletServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/wallet")
public class WalletMvcController {
    private final WalletServiceImpl walletService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public WalletMvcController(WalletServiceImpl walletService, AuthenticationHelper authenticationHelper) {
        this.walletService = walletService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/{id}")
    public String getWalletById(@PathVariable int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            Wallet wallet = walletService.getWalletById(id, user.getId());
            model.addAttribute("wallet", wallet);
            return ""; // return the view for displaying a single wallet
        } catch (EntityNotFoundException | AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "";
        }
    }

    @GetMapping("/user/{userId}")
    public String getWalletByUserId(@PathVariable int userId, HttpSession session, Model model) {
        try {
            User authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
            Wallet wallet = walletService.getWalletByUserId(authenticatedUser, userId);
            model.addAttribute("wallet", wallet);
            return "";
        } catch (EntityNotFoundException | AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "";
        }
    }

    @PostMapping
    public String createWallet(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            Wallet wallet = new Wallet();
            wallet.setUser(user);
            wallet.setMoneyBalance(0.0);
            walletService.create(wallet);
            model.addAttribute("wallet", wallet);
            return "";
        } catch (EntityDuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "errorPage";
        }
    }

    @PutMapping("/{id}")
    public String updateWallet(@PathVariable int id, @RequestParam double newBalance, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            Wallet wallet = new Wallet();
            wallet.setId(id);
            wallet.setMoneyBalance(newBalance);
            walletService.update(wallet, user.getId());
            model.addAttribute("wallet", wallet);
            return "";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errorPage";
        }
    }

    @DeleteMapping("/{id}")
    public String deleteWallet(@PathVariable int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            walletService.delete(id, user.getId());
            return "walletDeleted";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errorPage";
        }
    }
}