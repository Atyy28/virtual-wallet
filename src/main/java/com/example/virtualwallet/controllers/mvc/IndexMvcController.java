package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.services.interfaces.CardService;
import com.example.virtualwallet.services.interfaces.UserService;
import com.example.virtualwallet.services.interfaces.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/index")
public class IndexMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final WalletService walletService;
    private final CardService cardService;

    @Autowired
    public IndexMvcController(UserService userService, WalletService walletService, AuthenticationHelper authenticationHelper,
                              CardService cardService){
        this.userService = userService;
        this.walletService = walletService;
        this.authenticationHelper = authenticationHelper;
        this.cardService = cardService;
    }

    @GetMapping()
    public String showIndexPage(Model model, HttpSession session) {

        if (session.getAttribute("currentUser") == null) {
            return "redirect:/";
        }

        try {
            User user = authenticationHelper.tryGetCurrentUser(session);

            Wallet wallet = walletService.getWalletByUserId(user,user.getId());
            Card card = cardService.getCardByUserId(user, user.getId());

            model.addAttribute("wallet", wallet);
            model.addAttribute("user", user);
            model.addAttribute("card", card);

            return "index";
        } catch(AuthorizationException e) {
            return "redirect:/auth/login";
        } catch (Exception e) {
            return "ErrorPage";
        }
    }
}