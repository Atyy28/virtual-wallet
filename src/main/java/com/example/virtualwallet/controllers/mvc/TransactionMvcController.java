package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.FilterOptions;
import com.example.virtualwallet.helpers.TransactionMapper;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.TransactionDto;
import com.example.virtualwallet.models.dtos.TransactionFilterDto;
import com.example.virtualwallet.services.interfaces.TransactionService;
import com.example.virtualwallet.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/transactions")
public class TransactionMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final TransactionMapper transactionMapper;
    private final TransactionService transactionService;
    private final UserService userService;

    @Autowired
    public TransactionMvcController(AuthenticationHelper authenticationHelper,
                                    TransactionMapper transactionMapper,
                                    TransactionService transactionService,
                                    UserService userService) {
        this.authenticationHelper = authenticationHelper;
        this.transactionMapper = transactionMapper;
        this.transactionService = transactionService;
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String showAllTransactionsForUser(@ModelAttribute("filter") TransactionFilterDto filter, Model model, HttpSession session) {
        FilterOptions filterOptions = new FilterOptions(
                filter.getStartDate(),
                filter.getEndDate(),
                filter.getSender(),
                filter.getRecipient(),
                filter.getSortBy(),
                filter.getSortOrder()
        );

        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }

        List<Transaction> transactions = transactionService.getAllTransactionsForUser(user.getUsername(), filterOptions);
        model.addAttribute("transactions", transactions);
        model.addAttribute("filter", filter);
        //return "index";
        return "TransactionsView";
    }

    @GetMapping("/new")
    public String showNewTransactionPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "";
        }

        model.addAttribute("transaction", new TransactionDto());
        return "";
    }

    @PostMapping("/new")
    public String createTransaction(@Valid @ModelAttribute("transaction") TransactionDto transactionDto,
                                    BindingResult bindingResult,
                                    Model model,
                                    HttpSession session) {
        User sender;
        try {
            sender = authenticationHelper.tryGetCurrentUser(session);
        } catch (AuthorizationException e) {
            return "";
        }

        if (bindingResult.hasErrors()) {
            return "";
        }

        try {
            Transaction transaction = transactionMapper.fromDto(transactionDto);
            User recipient = userService.getUserByIdentifier(transactionDto.getRecipientIdentifier());
            double amount = transactionDto.getAmount();
            transactionService.create(sender, recipient, amount, transaction);
            return "";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "";
        }
    }
}