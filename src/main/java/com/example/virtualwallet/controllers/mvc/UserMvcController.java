package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityDuplicateException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.UserMapper;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.UserDto;
import com.example.virtualwallet.models.dtos.UserDtoForUpdate;
import com.example.virtualwallet.services.interfaces.TransactionService;
import com.example.virtualwallet.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/users")
public class UserMvcController {
    private final UserService userService;
    private final TransactionService transactionService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserMvcController(UserService userService,
                             TransactionService transactionService, UserMapper userMapper,
                             AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.transactionService = transactionService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public String showAllUsers(Model model, HttpSession session) {
        try {
            User authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
            List<User> users = userService.getAllUsers(authenticatedUser);
            model.addAttribute("users", users.stream().map(userMapper::toDto).collect(Collectors.toList()));
            return "";
        } catch (AuthorizationException | EntityDuplicateException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errorPage";
        }
    }

    @GetMapping("/{id}")
    public String showSingleUser(@PathVariable int id, Model model) {
        try {
            User user = userService.getUserById(id);
            model.addAttribute("user", user);
            model.addAttribute("user", userService.getUserById(id));
            return "";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "errorPage";

        }
    }

    @GetMapping("/profile")
    public String showUserProfile(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            user = userService.getByUsername(user.getUsername());
            model.addAttribute("user", user);
            return "MyProfile";
        } catch (EntityNotFoundException | AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "index";
        }
    }

    @GetMapping("/create")
    public String showCreateUserForm(Model model) {
        model.addAttribute("userDto", new UserDto());
        return "createUser";
    }

    @PostMapping("/create")
    public String createUser(@Valid UserDto userDto, Model model) {
        try {
            User user = userMapper.fromDto(userDto);
            userService.create(user);
            return "redirect:/users";
        } catch (EntityDuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "createUser";
        }
    }

    @GetMapping("/{id}/update")
    public String showUpdateUserForm(@PathVariable int id, Model model) {
        try {
            User user = userService.getUserById(id);
            UserDto userDto = userMapper.toDto(user);
            model.addAttribute("userDto", userDto);
            return "updateUser";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id, @Valid UserDtoForUpdate userDtoForUpdate, Model model, HttpSession session) {
        try {
            User authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
            User user = userMapper.fromDtoForUpdate(userDtoForUpdate);
            user.setId(id);
            userService.update(user, authenticatedUser);
            return "redirect:/users";
        } catch (EntityNotFoundException | EntityDuplicateException | AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "updateUser";
        }
    }

    @GetMapping("/{id}/block")
    public String blockUser(@PathVariable int id, Model model, HttpSession session) {
        try {
            User admin = authenticationHelper.tryGetCurrentUser(session);
            User user = userService.getUserById(id);
            userService.blockUser(user, admin);
            return "redirect:/users";
        } catch (EntityNotFoundException | EntityDuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/{id}/unblock")
    public String unblockUser(@PathVariable int id, Model model, HttpSession session) {
        try {
            User admin = authenticationHelper.tryGetCurrentUser(session);
            User user = userService.getUserById(id);
            userService.unblockUser(user, admin);
            return "redirect:/users";
        } catch (EntityNotFoundException | EntityDuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/search")
    public String searchUser(
            @RequestParam(required = false) String username,
            @RequestParam(required = false) String email,
            @RequestParam(required = false) String phoneNumber,
            Model model, HttpSession session) {
        try {
            User authenticatedUser = authenticationHelper.tryGetCurrentUser(session);
            User user = userService.searchUser(username, email, phoneNumber, authenticatedUser);
            model.addAttribute("user", user);
            return "";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "searchForm";
        } catch (AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }
}