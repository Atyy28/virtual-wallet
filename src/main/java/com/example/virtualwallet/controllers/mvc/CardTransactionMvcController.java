package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityDuplicateException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.CardTransactionMapper;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.CardTransaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.dtos.CardTransactionDto;
import com.example.virtualwallet.services.interfaces.CardService;
import com.example.virtualwallet.services.interfaces.CardTransactionService;
import com.example.virtualwallet.services.interfaces.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/card-transaction")
public class CardTransactionMvcController {
    private final CardTransactionService cardTransactionService;
    private final WalletService walletService;
    private final CardService cardService;
    private final AuthenticationHelper authenticationHelper;
    private final CardTransactionMapper cardTransactionMapper;

    @Autowired
    public CardTransactionMvcController(CardTransactionService cardTransactionService,
                                        WalletService walletService,
                                        CardService cardService,
                                        AuthenticationHelper authenticationHelper,
                                        CardTransactionMapper cardTransactionMapper) {
        this.cardTransactionService = cardTransactionService;
        this.walletService = walletService;
        this.cardService = cardService;
        this.authenticationHelper = authenticationHelper;
        this.cardTransactionMapper = cardTransactionMapper;
    }

    @GetMapping
    public String getAllCardTransactions(Model model) {
        try {
            List<CardTransaction> transactions = cardTransactionService.getAllCardTransactions();
            model.addAttribute("transactions", transactions);
            return "";
        } catch (Exception e) {
            model.addAttribute("error", "Unable to fetch card transactions.");
            return "ErrorPage";
        }
    }

    @GetMapping("/{id}")
    public String getCardTransactionById(@PathVariable int id, Model model) {
        try {
            CardTransaction transaction = cardTransactionService.getCardTransactionById(id);
            model.addAttribute("transaction", transaction);
            return "";
        } catch (EntityNotFoundException | AuthorizationException e) {
            model.addAttribute("error", e.getMessage());
            return "ErrorPage";
        }
    }

    @GetMapping("/deposit")
    public String getDeposit(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("amount", new CardTransactionDto());
            return "DepositView";
        } catch (AuthorizationException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/deposit")
    public String deposit(@Valid @ModelAttribute("amount") CardTransactionDto amount, BindingResult bindingResult, HttpSession session,
                          Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Please enter a valid amount."); // Add a generic error message
            return "DepositView";
        }

        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            Wallet wallet = walletService.getWalletByUserId(user, user.getId());
            Card card = cardService.getCardByUserId(user, user.getId());
            model.addAttribute("amount", amount);

            CardTransaction transaction = cardTransactionService.deposit(user, wallet, card,
                    cardTransactionMapper.fromDto(amount).getAmount());
            return "redirect:/index";
        } catch (EntityNotFoundException | AuthorizationException | EntityDuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "DepositView";
        }
    }
}