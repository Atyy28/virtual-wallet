package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.EntityDuplicateException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.CardMapper;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.CardDto;
import com.example.virtualwallet.services.interfaces.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/card")
public class CardMvcController {
    private final CardService cardService;
    private final CardMapper cardMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CardMvcController(CardService cardService,
                             CardMapper cardMapper,
                             AuthenticationHelper authenticationHelper) {
        this.cardService = cardService;
        this.cardMapper = cardMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/create")
    public String showCreateCardForm(Model model) {
        model.addAttribute("cardDto", new CardDto());
        return "CardView";
    }

    @PostMapping("/create")
    public String createCard(@Valid CardDto cardDto,
                             Model model,
                             HttpSession httpSession) {
        try {
            User lastRegisteredUser = (User) httpSession.getAttribute("lastRegisteredUser");

            if (lastRegisteredUser == null) {
                model.addAttribute("error", "No recent registered user found.");
                return "ErrorPage";
            }

            Card card = cardMapper.fromDto(cardDto);
            cardService.create(card, lastRegisteredUser);
            model.addAttribute("cardDto", cardDto);
            return "redirect:/auth/login";
        } catch (EntityDuplicateException e) {
            model.addAttribute("error", e.getMessage());
            return "ErrorPage";
        }
    }
}