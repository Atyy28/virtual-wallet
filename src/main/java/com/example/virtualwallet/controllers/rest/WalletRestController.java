package com.example.virtualwallet.controllers.rest;

import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityDuplicateException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.WalletMapper;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.dtos.CardDto;
import com.example.virtualwallet.models.dtos.WalletDto;
import com.example.virtualwallet.services.interfaces.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/wallet")
public class WalletRestController {
    public static final String UNABLE_TO_FETCH_ERROR = "Unable to fetch all cards";
    private final WalletService walletService;
    private final WalletMapper walletMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public WalletRestController(WalletService walletService,
                                WalletMapper walletMapper,
                                AuthenticationHelper authenticationHelper) {
        this.walletService = walletService;
        this.walletMapper = walletMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<WalletDto> getAllWallets() {
        try {
            List<Wallet> wallets = walletService.getAllWallets();
            return wallets.stream().map(walletMapper::toDto).collect(Collectors.toList());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, UNABLE_TO_FETCH_ERROR);
        }
    }

    @GetMapping("/{id}")
    public Wallet getWalletById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            return walletService.getWalletById(id, authenticatedUser.getId());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/user/{userId}")
    public Wallet getWalletByUserId(@RequestHeader HttpHeaders headers, @PathVariable int userId) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            return walletService.getWalletByUserId(authenticatedUser, userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping
    public Wallet createWallet(@RequestBody WalletDto walletDto) {
        try {
            Wallet wallet = walletMapper.fromDto(walletDto);
            walletService.create(wallet);
            return wallet;
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Wallet updateWallet(@RequestHeader HttpHeaders headers, @PathVariable int id, @RequestBody WalletDto walletDto) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            Wallet wallet = walletMapper.fromDtoWithId(walletDto);
            wallet.setId(id);
            walletService.update(wallet, authenticatedUser.getId());
            return wallet;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteWallet(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            walletService.delete(id, authenticatedUser.getId());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}