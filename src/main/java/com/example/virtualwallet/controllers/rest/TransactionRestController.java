package com.example.virtualwallet.controllers.rest;

import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.FilterOptions;
import com.example.virtualwallet.helpers.TransactionMapper;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.TransactionDto;
import com.example.virtualwallet.services.interfaces.TransactionService;
import com.example.virtualwallet.services.interfaces.UserService;
import com.example.virtualwallet.services.interfaces.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/transactions")
public class TransactionRestController {

    private final TransactionService transactionService;
    private final TransactionMapper transactionMapper;
    private final AuthenticationHelper authenticationHelper;
    private final WalletService walletService;
    private final UserService userService;

    @Autowired
    public TransactionRestController(TransactionService transactionService, TransactionMapper transactionMapper, AuthenticationHelper authenticationHelper, WalletService walletService, UserService userService) {
        this.transactionService = transactionService;
        this.transactionMapper = transactionMapper;
        this.authenticationHelper = authenticationHelper;
        this.walletService = walletService;
        this.userService = userService;
    }

    @GetMapping("/forUser")
    public List<TransactionDto> getAllTransactionsForUser(@RequestHeader HttpHeaders headers,
            @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
            @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate,
            @RequestParam(required = false) String sender,
            @RequestParam(required = false) String recipient,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortOrder
    ) {
        try {
            String senderUsername = authenticationHelper.tryGetUser(headers).getUsername();
            FilterOptions filterOptions = new FilterOptions(startDate, endDate, sender, recipient, sortBy, sortOrder);
            List<Transaction> transactions = transactionService.getAllTransactionsForUser(senderUsername, filterOptions);

            if (sender != null) {
                transactions = transactions.stream()
                        .filter(transaction -> {
                            User transactionSender = transaction.getSender();
                            return transactionSender != null && sender.equals(transactionSender.getUsername());
                        })
                        .collect(Collectors.toList());
            }

            return transactions.stream()
                    .map(transactionMapper::toDto)
                    .collect(Collectors.toList());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping
    public List<TransactionDto> getAllTransactions(
            @RequestHeader HttpHeaders headers,
            @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
            @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate,
            @RequestParam(required = false) String sender,
            @RequestParam(required = false) String recipient,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortOrder
    ) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            FilterOptions filterOptions = new FilterOptions(startDate, endDate, sender, recipient, sortBy, sortOrder);
            List<Transaction> transactions = transactionService.getAllTransactions(authenticatedUser,filterOptions);
            return transactions.stream()
                    .map(transactionMapper::toDto)
                    .collect(Collectors.toList());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "An error occurred while processing your request.");
        }
    }

    @GetMapping("/{transactionId}")
    public Transaction getById(@PathVariable int transactionId) {
        try {
            //Transaction transaction = transactionService.getById(transactionId);
            return transactionService.getById(transactionId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Transaction create(@RequestHeader HttpHeaders headers, @Valid @RequestBody TransactionDto transactionDto) {
        try {
            User sender = authenticationHelper.tryGetUser(headers);
            User receiver = userService.getUserByIdentifier(transactionDto.getRecipientIdentifier());
            double amount = transactionDto.getAmount();
            Transaction transaction = transactionMapper.fromDto(transactionDto);

            transactionService.create(sender, receiver, amount, transaction);

            return transaction;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}