package com.example.virtualwallet.controllers.rest;

import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityDuplicateException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.CardTransactionMapper;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.CardTransaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.dtos.CardTransactionDto;
import com.example.virtualwallet.services.interfaces.CardService;
import com.example.virtualwallet.services.interfaces.CardTransactionService;
import com.example.virtualwallet.services.interfaces.WalletService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/card/transaction")
public class CardTransactionRestController {
    public static final String MESSAGE_UNABLE_TO_FETCH = "Unable to fetch card transactions for the recipient";
    public static final String MESSAGE_UNABLE_ERROR = "Unable to fetch cardTransactions";
    private final CardTransactionService cardTransactionService;
    private final WalletService walletService;
    private final CardService cardService;
    private final CardTransactionMapper cardTransactionMapper;
    private final AuthenticationHelper authenticationHelper;

    public CardTransactionRestController(CardTransactionService cardTransactionService,
                                         WalletService walletService,
                                         CardService cardService,
                                         CardTransactionMapper cardTransactionMapper,
                                         AuthenticationHelper authenticationHelper) {
        this.cardTransactionService = cardTransactionService;
        this.walletService = walletService;
        this.cardService = cardService;
        this.cardTransactionMapper = cardTransactionMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<CardTransactionDto> getAllCardTransactions() {
        try {
            List<CardTransaction> cardTransaction = cardTransactionService.getAllCardTransactions();
            return cardTransaction.stream().map(cardTransactionMapper::toDto).collect(Collectors.toList());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, MESSAGE_UNABLE_ERROR);
        }
    }

    @GetMapping("/{id}")
    public CardTransactionDto getCardTransactionById(@PathVariable int id) {
        try {
            CardTransaction transaction = cardTransactionService.getCardTransactionById(id);
            return cardTransactionMapper.toDto(transaction);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/recipient/{recipientId}")
    public List<CardTransactionDto> getCardTransactionsByRecipientId(@PathVariable int recipientId) {
        try {
            List<CardTransaction> transactions = cardTransactionService.getCardTransactionByRecipientId(recipientId);
            return transactions.stream().map(cardTransactionMapper::toDto).collect(Collectors.toList());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, MESSAGE_UNABLE_TO_FETCH);
        }
    }

    @PostMapping("/deposit")
    public CardTransaction deposit(@RequestHeader HttpHeaders headers, @RequestParam double amount) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Wallet wallet = walletService.getWalletByUserId(user, user.getId());
            Card card = cardService.getCardByUserId(user, user.getId());
            return cardTransactionService.deposit(user, wallet, card, amount);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}