package com.example.virtualwallet.controllers.rest;

import com.example.virtualwallet.exceptions.AuthorizationException;
import com.example.virtualwallet.exceptions.EntityDuplicateException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.CardMapper;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.CardDto;
import com.example.virtualwallet.services.interfaces.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/cards")
public class CardRestController {
    public static final String UNABLE_TO_FETCH_ERROR = "Unable to fetch all cards";
    private final CardService cardService;
    private final CardMapper cardMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CardRestController(CardService cardService,
                              CardMapper cardMapper,
                              AuthenticationHelper authenticationHelper) {
        this.cardService = cardService;
        this.cardMapper = cardMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<CardDto> getAllCards() {
        try {
            List<Card> cards = cardService.getAllCards();
            return cards.stream().map(cardMapper::toDto).collect(Collectors.toList());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, UNABLE_TO_FETCH_ERROR);
        }
    }

    @GetMapping("/{id}")
    public Card getCardById(@PathVariable int id) {
        try {
            return cardService.getCardById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{userId}")
    public Card getCardByUserId(@RequestHeader HttpHeaders headers, @PathVariable int userId) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            return cardService.getCardByUserId(authenticatedUser, userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping
    public Card create(@Valid @RequestBody CardDto cardDto, @RequestHeader HttpHeaders headers) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            Card card = CardMapper.fromDto(cardDto);
            cardService.create(card, authenticatedUser);
            return card;
        } catch (EntityDuplicateException | AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Card update(@PathVariable int id, @Valid @RequestBody CardDto cardDto, @RequestHeader HttpHeaders headers) {
        User authenticatedUser = authenticationHelper.tryGetUser(headers);
        try {
            Card cardToUpdate = CardMapper.fromDto(cardDto);
            cardService.update(id, cardToUpdate, authenticatedUser);
            return cardToUpdate;
        } catch (EntityDuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, User user) {
        try {
            cardService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}