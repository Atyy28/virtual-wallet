INSERT INTO users (username, password, email, phone_number, photo_url, first_name, last_name, is_admin, is_blocked, is_deactivated)
VALUES
    ('john_doe', 'pass1234', 'john.doe@example.com', '1234567890', '/img/vito.jpg', 'John', 'Doe', 0, 0, 0),
    ('jane_smith', 'pass5678', 'jane.smith@example.com', '9876543210', 'https://example.com/jane_smith.jpg', 'Jane', 'Smith', 1, 0, 0),
    ('robert_brown', 'pass4321', 'robert.brown@example.com', '1122334455', 'https://example.com/robert_brown.jpg', 'Robert', 'Brown', 0, 0, 0),
    ('emily_jones', 'pass6789', 'emily.jones@example.com', '2233445566', 'https://example.com/emily_jones.jpg', 'Emily', 'Jones', 0, 0, 0),
    ('michael_white', 'pass9999', 'michael.white@example.com', '3344556677', 'https://example.com/michael_white.jpg', 'Michael', 'White', 0, 0, 0),
    ('sarah_green', 'pass1111', 'sarah.green@example.com', '4455667788', 'https://example.com/sarah_green.jpg', 'Sarah', 'Green', 0, 0, 0),
    ('paul_gray', 'pass2222', 'paul.gray@example.com', '5566778899', 'https://example.com/paul_gray.jpg', 'Paul', 'Gray', 0, 0, 0),
    ('rebecca_wilson', 'pass3333', 'rebecca.wilson@example.com', '6677889900', 'https://example.com/rebecca_wilson.jpg', 'Rebecca', 'Wilson', 0, 0, 0),
    ('anthony_peterson', 'pass4444', 'anthony.peterson@example.com', '7788990011', 'https://example.com/anthony_peterson.jpg', 'Anthony', 'Peterson', 0, 0, 0),
    ('olivia_taylor', 'pass5555', 'olivia.taylor@example.com', '8899001122', 'https://example.com/olivia_taylor.jpg', 'Olivia', 'Taylor', 0, 0, 0);

INSERT INTO wallet (money_balance, user_id)
VALUES
    (500.00, 1),
    (300.00, 2),
    (700.00, 3),
    (600.00, 4),
    (200.00, 5),
    (900.00, 6),
    (100.00, 7),
    (800.00, 8),
    (250.00, 9),
    (450.00, 10);

INSERT INTO cards (card_number, expiration_date, cvv, card_holder, user_id)
VALUES
    ('1234567890123456', '2024-12-31', '123', 'John Doe', 1),
    ('2345678901234567', '2025-01-31', '456', 'Jane Smith', 2),
    ('3456789012345678', '2023-11-30', '789', 'Robert Brown', 3),
    ('4567890123456789', '2024-01-31', '101', 'Emily Jones', 4),
    ('5678901234567890', '2023-05-31', '202', 'Michael White', 5),
    ('6789012345678901', '2024-08-31', '303', 'Sarah Green', 6),
    ('7890123456789012', '2023-09-30', '404', 'Paul Gray', 7),
    ('8901234567890123', '2025-12-31', '505', 'Rebecca Wilson', 8),
    ('9012345678901234', '2023-02-28', '606', 'Anthony Peterson', 9),
    ('0123456789012345', '2024-07-31', '707', 'Olivia Taylor', 10);

INSERT INTO card_transaction (date, recipient_id, amount)
VALUES
    ('2023-08-21 10:30:00', 1, 100.00),
    ('2023-08-21 11:00:00', 2, 50.00),
    ('2023-08-21 11:45:00', 3, 150.00),
    ('2023-08-21 12:30:00', 4, 75.00),
    ('2023-08-21 13:30:00', 5, 120.00);

INSERT INTO transactions (date, amount, sender_id, recipient_id)
VALUES
    ('2023-08-22 09:45:00', 45.00, 2, 1),
    ('2023-08-21 18:10:00', 65.00, 1, 3),
    ('2023-08-23 13:15:00', 90.00, 3, 2),
    ('2023-08-24 11:05:00', 35.00, 2, 4),
    ('2023-08-23 15:40:00', 55.00, 4, 3),
    ('2023-08-24 08:50:00', 60.00, 4, 1),
    ('2023-08-22 14:25:00', 40.00, 1, 4),
    ('2023-08-21 20:20:00', 95.00, 5, 6),
    ('2023-08-21 22:30:00', 85.00, 1, 5),
    ('2023-08-23 19:00:00', 75.00, 6, 7),
    ('2023-08-24 10:15:00', 15.00, 7, 1),
    ('2023-08-23 12:35:00', 20.00, 7, 8),
    ('2023-08-24 17:05:00', 50.00, 1, 7),
    ('2023-08-21 21:45:00', 55.00, 8, 9),
    ('2023-08-23 09:30:00', 70.00, 9, 1),
    ('2023-08-22 20:55:00', 95.00, 9, 10),
    ('2023-08-21 19:25:00', 35.00, 10, 9),
    ('2023-08-24 14:10:00', 45.00, 10, 1);

