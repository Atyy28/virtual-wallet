drop database if exists virtual_wallet;
create database virtual_wallet;
use virtual_wallet;

create table users
(
    id             int auto_increment
        primary key,
    username       varchar(20)  not null,
    password       varchar(50)  not null,
    email          varchar(255) not null,
    phone_number   varchar(10)  not null,
    photo_url      varchar(255) not null,
    first_name     varchar(20)  not null,
    last_name      varchar(20)  not null,
    is_admin       tinyint(1)   not null,
    is_blocked     tinyint(1)   not null,
    is_deactivated tinyint(1)   not null
);

create table card_transaction
(
    id           int auto_increment
        primary key,
    date         datetime not null,
    recipient_id int      not null,
    amount       double   not null,
    constraint card_transaction_users_id_fk
        foreign key (recipient_id) references users (id)
);

create table cards
(
    id              int auto_increment
        primary key,
    card_number     varchar(16) not null,
    expiration_date date        not null,
    cvv             varchar(3)  not null,
    card_holder     varchar(30) not null,
    user_id         int         not null,
    constraint cards_pk
        unique (user_id),
    constraint cards_users_id_fk
        foreign key (user_id) references users (id)
);

create table transactions
(
    id           int auto_increment
        primary key,
    date         datetime not null,
    amount       double   not null,
    sender_id    int      null,
    recipient_id int      null,
    constraint transactions_users_user_id_fk
        foreign key (sender_id) references users (id),
    constraint transactions_users_user_id_fk2
        foreign key (recipient_id) references users (id)
);

create table wallet
(
    money_balance double not null,
    id            int auto_increment
        primary key,
    user_id       int    not null,
    constraint wallet_pk
        unique (user_id),
    constraint wallet_users_id_fk
        foreign key (user_id) references users (id)
);

