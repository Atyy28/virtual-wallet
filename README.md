# VirtuPay - Virtual Wallet Web Application

## Project Description

VirtuPay is a web application that allows the users to put their money into a virtual wallet. They can deposit money from a credit/debit card and send money to other users.

The application consists of the following main parts:

## Swagger Documentation

To explore and interact with the API endpoints, please visit the [Swagger documentation](http://localhost:8080/swagger-ui/).